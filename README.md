# genTree

A jQuery plugin that displays genealogy data as a tree in a canvas.

- - -
![genTree example](https://bitbucket.org/ljohansson/gentree/raw/master/advancedExample1.png)
- - -

## Features

* Displays genealogy data as a tree. 
* Shows the current person, parents, grandparents, spouse, children and siblings with firstname and last name.
* Can switch between displaying children or siblings.
* Supports multiple families
* Supports multiple parents (i.e biological parents and adoptive parents)
* Each person is clickable with an url to another web page.
* Each family and parent line is clickable with an url to another web page.
* Has a very basic fallback for browsers that don't support canvas. And displays the data in a html-table.

## Requirements

* jQuery.
* A modern web browser that supports canvas.

## Usage

* Place genealogy data in a JSON-object inside a hidden html-element.

```javascript
<div id="simpleGendata" style="display: none;">   
{   
"person": { "firstName": "Me", "lastName": "Myself and I", "gender": "M", "isCurrent": "true", "url": "example.html" },   
"parents": [   
    { "url": "../families/F0000.html",   
	"father": { "firstName": "My", "lastName": "Father", "gender": "M", "url": "I0001.html" },   
	"fathersParents": { "url": "../families/F0002.html" ,   
		"father": { "firstName": "My Fathers", "lastName": "Father", "gender": "M", "url": "I0007.html" },   
		"mother": { "firstName": "My Fathers", "lastName": "Mother", "gender": "F", "url": "I0008.html" } } ,   
	"mother": { "firstName": "My", "lastName": "Mother", "gender": "F", "url": "I0002.html" },   
	"mothersParents": { "url": "../families/F0001.html" ,   
		"father": { "firstName": "My Mothers", "lastName": "Father", "gender": "M", "url": "I0005.html" },   
		"mother": { "firstName": "My Mothers", "lastName": "Mother", "gender": "F", "url": "I0006.html" } } ,   
	"siblings": [   
		{ "firstName": "My", "lastName": "Brother", "gender": "M", "url": "I0003.html" },  
		{ "firstName": "Me", "lastName": "Myself and I", "gender": "M", "isCurrent": "true", "url": "example.html" }   
		]   
	}	  
	],   
"families": [   
	{ "url": "../families/F0009.html",   
	"spouse": { "firstName": "My", "lastName": "First wife", "gender": "F", "url": "I0027.html" },   
	"children": [   
		{ "firstName": "My", "lastName": "Daughter", "gender": "F", "url": "I0999.html" },   
		{ "firstName": "My", "lastName": "Son", "gender": "M", "url": "I1000.html" }   
		]   
	}  
	]   
}  
</div>
```

* Include jQuery and the genTree plugin.
* Call the plugin on the div-element with jQuery.

```javascript
<script type="text/javascript" src="https://code.jquery.com/jquery-1.6.2.min.js"></script>  
<script type="text/javascript" src="jquery.genTree.js"></script>  
<script type="text/javascript">  
$(function()   
{  
	$("#simpleGendata").genTree();  
});  
</script>
```

## JSON format

* person
    * firstName
    * lastName
    * gender
    * url
    * isCurrent
* parents (array)
    * url
    * father
        * Same attributes as person
    * fathersParents
        * father
            * Same attributes as person
        * mother
            * Same attributes as person
    * mother
        * Same attributes as person
    * mothersParents
        * father
            * Same attributes as person
        * mother
            * Same attributes as person
    * siblings (array)
        * Each sibling object has the same attributes as person
* families (array)
    * url
    * spouse
        * Same attributes as person
    * children (array)
        * Each child object has the same attributes as person

### Person
Attribute | Required | Description
--------- | -------- | -----------
**person** | **yes** | **Object that describes the current person. See attributes below**
firstName | yes | The firstname of the person
lastName | yes | The lastname of the person
gender | no | "M" or "F" draws the name in the male or female color. Anything else draws the name in the unknown color.
url | yes | When clicking on the person name, the browser will be redirected to this url (e.g a person detail page). Empty string if not used.
isCurrent | no | Set to anything (e.g true) and the name will be drawn in a larger font.

### Parents
Attribute | Required | Description
--------- | -------- | -----------
**parents** | **no** | **Array of objects with the attributes below.**
url | yes | When clicking on the line between the parents, the browser will be redirected to this url (e.g family detail page). Empty string if not used.
father | no | A person object. Se above
fathersParents | no | Object whith the attributes url, father and mother
mother | no | A person object. Se above
mothersParents | no | Object whith the attributes url, father and mother
siblings | no | Array of person objects

### Families
Attribute | Required | Description
--------- | -------- | -----------
**families** | **no** | **Array of objects with the attributes below.**
url | yes | When clicking on the line between the person and the spouse, the browser will be redirected to this url (e.g family detail page). Empty string if not used.
spouse | no | A person object. Se above
children | yes | Array of person objects


## Plugin options

Option | Default value | Description
------ | ------------- | -----------
cvHeight | 250 | The height of the generated canvas
cvWidth | 600 | The width of the generated canvas
maleColor | "#0000cc" | Text color of male persons
femaleColor | "#cc0000" | Text color of female persons
unknownColor | "#000000" | Text color of persons with unknown gender
lineColor | "#dddddd" | Color of lines
buttonColor | "#aaaaaa" | Color of checkbox and radiobuttons
showChildNo | true | Flag to control if children name is preceded with a number
rowHeight | 50 | Height between rows of persons
hMargin | 30 | Horizontal margin around persons
grandParentHMargin | 100 | Horizontal marigin between center line and grandparents