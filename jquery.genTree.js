/**
* genTree - A jQuery plugin that displays genealogy data as a tree in a canvas.
* 
* Version 1.0.0
*
* Copyright (C) 2012 by Lars Johansson
*
* Licensed under the MIT license
*/

(function($){
	$.fn.genTree = function(options) {  
		var settings = {
			cvHeight: 250,
			cvWidth: 600,
			maleColor: "#0000cc",
			femaleColor: "#cc0000",
			unknownColor: "#000000",
			lineColor: "#dddddd",
			buttonColor: "#aaaaaa",
			showChildNo: true,
			rowHeight: 50,
			hMargin: 30,
			grandParentHMargin: 100
		};
		$.extend(settings, options);
		
		// Canvas Variables
	    var cv, ctx;
		
		//get data
		var dataSourceId = $(this).attr("id");
		var dataSourceJSONStr = $(this).text();
		var genData = $.parseJSON(dataSourceJSONStr);
		
		var persons = new Array();
		var families = new Array();
		var familyButtons = new RadioButtonGroup();
		var parentsButtons = new RadioButtonGroup();
		var siblingButton;
		var currentFamily = 0;
		var currentParents = 0;
		var showSiblings = false;
		
		initCanvas();
		if(ctx)
		{
			draw();
		}
		else
		{
			fallbackTable();
		}
		
		function initCanvas()
		{
			//inject canvas
			$("#"+dataSourceId).after("<canvas id=\""+dataSourceId+"-canvas\" class=\"genTreeCanvas\"> </canvas>");

			// Try to access the canvas element 
			cv = $("#"+dataSourceId+"-canvas").get(0);

			cv.height=settings.cvHeight;
			cv.width=settings.cvWidth;
			
			//setup click handling
			$("#"+dataSourceId+"-canvas").click(function(e){
				var posx = e.pageX-$("#"+dataSourceId+"-canvas").offset().left;
				var posy = e.pageY-$("#"+dataSourceId+"-canvas").offset().top;
				
				var clicked = handlePersonClick(posx, posy);
				if(!clicked)
					clicked = handleFamilyClick(posx, posy);
				if(!clicked)
					handleButtonClick(posx, posy);
			});
			
			if (!cv.getContext) 
			{ return; }
		 
			// Try to get a 2D context for the canvas and throw an error if unable to
			ctx = cv.getContext('2d');
		}
		
		function fallbackTable()
		{
			//remove canvas tag
			$("#"+dataSourceId+"-canvas").remove();
			
			var father = "";
			var mother = "";
			var person = "";
			var familiesStr = "";
			if(genData.parents.length > 0)
			{
				parents = genData.parents[0];
				if(parents.father)
					father = "Father: <a href=\""+parents.father.url+"\">"+parents.father.firstName+" "+parents.father.lastName+"</a>";
				if(parents.mother)
					mother = "Mother: <a href=\""+parents.mother.url+"\">"+parents.mother.firstName+" "+parents.mother.lastName+"</a>";
			}
			if(genData.person)
				person = "Person: <a href=\""+genData.person.url+"\">"+genData.person.firstName+" "+genData.person.lastName+"</a>";
			
			for(var i = 0; i < genData.families.length; i++)
			{
				var familyStr = "<table>";
				family = genData.families[i];
				
				var spouse = "";
				if(family.spouse)
					spouse += "Spouse "+(i+1)+": <a href=\""+family.spouse.url+"\">"+family.spouse.firstName+" "+family.spouse.lastName+"</a>";
				familyStr += "<tr><td>"+spouse+"</td></tr>"
				
				familyStr += "<tr>";
				var children = "<td>children:</td>";
				for(var j = 0; j < family.children.length; j++)
				{
					children += "<td>" + "<a href=\""+family.children[j].url+"\">" + family.children[j].firstName+" "+family.children[j].lastName + "</a></td>";
				}
				familyStr += children;
				familyStr += "<tr>";
				familiesStr += familyStr;
			}
				
			var parentTag = $("#"+dataSourceId).parent();
			parentTag.append("<div>Your browser does not support html5 canvas! Otherwise a familytree would be displayed here.</div>");
			parentTag.append("<table><tr><td>"+father+"</td><td>"+mother+"</td></tr>"
				+"</table>"
				+"<table>"
				+"<tr><td>"+person+"</td></tr>"
				+"</table>"
				+familiesStr);
		}
		
		function clear()
		{
			persons = new Array();
			families = new Array();
			familyButtons = new RadioButtonGroup();
			parentsButtons = new RadioButtonGroup();
			ctx.save();
			
			//clear background
			ctx.clearRect(0, 0, settings.cvWidth, cv.height);
			
			ctx.restore();
		}
		
		function draw()
		{
			ctx.save();
			
			var rowHeight = settings.rowHeight;
			var hMargin = settings.hMargin;
			var grandParentHMargin = settings.grandParentHMargin;
			var hCenter = settings.cvWidth / 2;
			var grandParentRow = rowHeight;
			var parentRow = grandParentRow + rowHeight;
			var personRow = parentRow + rowHeight;
			var childRow = personRow + rowHeight;
			
			var familyMode = false;
			
			ctx.strokeStyle = settings.lineColor;
			ctx.lineWidth = 2;
			
			//init parent for drawParentLines
			var father = new Person(undefined, ctx, 'left');
			var mother = new Person(undefined, ctx, 'right');
			
			if(!genData.person)
			{
				//if person is not defined. Then in family mode.
				familyMode = true;
				showSiblings = true;
			}
			
			//parents
			if(genData.parents && genData.parents.length > 0)
			{
				var parents = genData.parents[currentParents];
				
				//father
				father = new Person(parents.father, ctx, 'left');
				father.position(hCenter - hMargin, parentRow);
				persons.push(father);
				//grandparents
				if(parents.fathersParents)
				{
					var fatherFather = new Person(parents.fathersParents.father, ctx, 'left');
					fatherFather.position(father.x - hMargin - grandParentHMargin, grandParentRow);
					persons.push(fatherFather);
					var fatherMother = new Person(parents.fathersParents.mother, ctx, 'right');
					fatherMother.position(father.x + hMargin - grandParentHMargin, grandParentRow);
					persons.push(fatherMother);
				}
				else
				{
					var fatherFather = new Person(undefined, ctx, 'left');
					var fatherMother = new Person(undefined, ctx, 'right');
				}
				
				//mother
				mother = new Person(parents.mother, ctx, 'right');
				mother.position(hCenter + hMargin, parentRow);
				persons.push(mother);
				//grandparents
				if(parents.mothersParents)
				{
					var motherFather = new Person(parents.mothersParents.father, ctx, 'left');
					motherFather.position(mother.x - hMargin + grandParentHMargin, grandParentRow);
					persons.push(motherFather);
					var motherMother = new Person(parents.mothersParents.mother, ctx, 'right');
					motherMother.position(mother.x + hMargin + grandParentHMargin, grandParentRow);
					persons.push(motherMother);
				}
				else
				{
					var motherFather = new Person(undefined, ctx, 'left');
					var motherMother = new Person(undefined, ctx, 'right');
				}
				
				if(showSiblings)
				{
					if(parents.siblings)
					{
						//siblings
						var siblings = new Array();
						createChildren(parents.siblings, siblings, personRow, rowHeight, hMargin, hCenter);
						parentsUrl = undefined;
						if(parents)
							parentsUrl = parents.url;
						drawFamilyLines(father, mother, siblings, parentsUrl);
					}
					else	
					{
						//person
						var person = new Person(genData.person, ctx, 'center');
						person.position(hCenter, personRow);
						persons.push(person);
						parentsUrl = undefined;
						if(parents)
							parentsUrl = parents.url;
						drawParentLines(person, father, mother, parentsUrl);
					}
				}
				
				//draw lines
				fatherParentsUrl = undefined;
				if(parents.fathersParents)
					fatherParentsUrl = parents.fathersParents.url;
				drawFamilyLines(fatherFather, fatherMother, new Array(father), fatherParentsUrl);
				motherParentsUrl = undefined;
				if(parents.mothersParents)
					motherParentsUrl = parents.mothersParents.url;
				drawFamilyLines(motherFather, motherMother, new Array(mother), motherParentsUrl);
			}
			
			if(!showSiblings)
			{
				//person
				var person = new Person(genData.person, ctx, 'center');
				person.position(hCenter, personRow);
				persons.push(person);
				parentsUrl = undefined;
				if(parents)
					parentsUrl = parents.url;
				drawParentLines(person, father, mother, parentsUrl);
				
				//families
				var spouse = new Person(undefined, ctx, 'center');
				var children = new Array();
				var familyUrl = undefined;
				if(genData.families && genData.families.length > 0)
				{
					family = genData.families[currentFamily];
					familyUrl = family.url;
					
					//spouse
					alignment = '';
					sx = 0;
					if(family.spouse)
					{
						if(family.spouse.gender == 'M')
						{
							alignment = 'left';
							sx = person.left - 2*hMargin;
						}
						else
						{
							alignment = 'right';
							sx = person.right + 2*hMargin;
						}
					}
					spouse = new Person(family.spouse, ctx, alignment);
					spouse.position(sx, personRow);
					persons.push(spouse);
					
					//children
					createChildren(family.children, children, childRow, rowHeight, hMargin, hCenter);
				}
				drawFamilyLines(person, spouse, children, familyUrl);
			}//families
			
			//draw all persons
			persons.forEach(function(person)
			{
				person.draw();
			});
			
			var buttonX = 50;
			//families radio buttons
			if(!showSiblings && genData.families && genData.families.length > 1)
			{
				for(var i = 0; i < genData.families.length; i++)
				{
					hDistance = 5;
					y = 15;
					var button = new Button(ctx, buttonX, y, i, 'radio', 'F'+(i+1));
					familyButtons.addButton(button);
					if(i == currentFamily)
						button.setSelected(true);
					button.draw();
					buttonX = button.right + hDistance;
				}
				buttonX += 2* hDistance;//make extra space to next button group
			}
			
			//parents radio buttons
			if(genData.parents && genData.parents.length > 1)
			{
				for(var i = 0; i < genData.parents.length; i++)
				{
					hDistance = 5;
					y = 15;
					var button = new Button(ctx, buttonX, y, i, 'radio', 'P'+(i+1));
					parentsButtons.addButton(button);
					if(i == currentParents)
						button.setSelected(true);
					button.draw();
					buttonX = button.right + hDistance;
				}
				buttonX += 2* hDistance;//make extra space to next button group
			}
			
			//button for show siblings
			if(!familyMode && genData.parents && genData.parents[currentParents])
			{
				var siblings = genData.parents[currentParents].siblings;
				if(siblings && siblings.length > 0)
				{
					x = 10;
					y = 15;
					i = 0;
					siblingButton = new Button(ctx, x, y, i, 'checkbox', 'S');
					siblingButton.setSelected(showSiblings);
					siblingButton.draw();
				}
			}
			
			ctx.restore();
		}
		
		function createChildren(childObjects, childArray, childRowY, rowHeight, hMargin, hCenter)
		{
			var childrenTotalWidth = - hMargin;
			//calculate total width
			var childNo = 1;
			childObjects.forEach(function(childObj)
			{
					var child = new Person(childObj, ctx, 'center');
					if(settings.showChildNo)
						child.setNames(childNo + '. ' + child.firstName, child.lastName);
					persons.push(child);
					childArray.push(child);
					childrenTotalWidth += child.width + hMargin;
					childNo++;
			});
			var noOfChildRows = 1;
			noOfChildRows += Math.floor((childrenTotalWidth + hMargin) / settings.cvWidth);
			//position children
			var averageRowWidth = childrenTotalWidth / noOfChildRows;
			var childX = hCenter - (averageRowWidth / 2);
			for(var i = 0; i < childArray.length; i++)
			{
				var child = childArray[i];
				childY = childRowY + (i % noOfChildRows) * rowHeight;
				if(i == 0)
				{
					//move first child center 1/2 width right
					childX += child.width /2;
				}
				else if(i > 0 && i < noOfChildRows)
				{
					//first in child 2,3,4... row
					//move first in next row a half step right
					var firstChildPrevRow = childArray[i - 1];
					childX = firstChildPrevRow.x + hMargin;
					//childX += (child.width + hMargin) / noOfChildRows;
				}
				else if(i >= noOfChildRows)
				{
					//not the first child in the row
					var prevChildInRow = childArray[i - noOfChildRows];
					childX = prevChildInRow.x + hMargin + (prevChildInRow.width + child.width) / 2;
					var prevChild = childArray[i - 1];
					if(childX < prevChild.x + (hMargin / 2))
						childX = prevChild.x + hMargin;//be sure that next child is right of prev
				}
				child.position(childX, childY);
			}
		}
		
		function drawParentLines(person, father, mother, familyUrl)
		{
			hMargin = 3;
			vMargin = 12;
			//vertical
			if(father.exists || mother.exists)
			{
				ctx.moveTo(person.x, person.y - person.height);
				ctx.lineTo(person.x, father.y - vMargin);
				ctx.stroke();
				families.push(new Family(person.x, father.y - vMargin, familyUrl));
			}
			
			//horizontal
			if(father.exists)
			{
				ctx.moveTo(father.right + hMargin, father.y - vMargin);
				ctx.lineTo(person.x, mother.y - vMargin);
				ctx.stroke();
			}
			if(mother.exists)
			{
				ctx.moveTo(person.x, father.y - vMargin);
				ctx.lineTo(mother.left - hMargin, mother.y - vMargin);
				ctx.stroke();
			}
		}
		
		function drawFamilyLines(person, spouse, children, familyUrl)
		{
			hMargin = 3;
			vMargin = 12;
			//spouse horizontal
			var spouseCenter = -1;
			if(person.exists && spouse.exists)
			{
				if(spouse.gender == 'M')
				{
					ctx.moveTo(spouse.right + hMargin, spouse.y - vMargin);
					ctx.lineTo(person.left - hMargin, person.y - vMargin);
					ctx.stroke();
					spouseCenter = Math.floor(spouse.right + (person.left - spouse.right)/2);
				}
				else
				{
					ctx.moveTo(spouse.left - hMargin, spouse.y - vMargin);
					ctx.lineTo(person.right + hMargin, person.y - vMargin);
					ctx.stroke();
					spouseCenter = Math.floor(person.right + (spouse.left - person.right)/2);
				}
			}
			else if(person.exists && children.length > 0)
			{
				//spouse does not exist but there is children
				//draw half spouse horizontal
				horizontalLengt = 15;
				if(person.gender == 'M')
				{
					ctx.moveTo(person.right + hMargin, person.y - vMargin);
					ctx.lineTo(person.right + horizontalLengt, person.y - vMargin);
					ctx.stroke();
					spouseCenter = person.right + horizontalLengt;
				}
				else
				{
					ctx.moveTo(person.left - hMargin, person.y - vMargin);
					ctx.lineTo(person.left - horizontalLengt, person.y - vMargin);
					ctx.stroke();
					spouseCenter = person.left - horizontalLengt;
				}
			}
			else if(spouse.exists && children.length > 0)
			{
				//person does not exist but there is children
				//draw half spouse horizontal
				horizontalLengt = 15;
				if(spouse.gender == 'M')
				{
					ctx.moveTo(spouse.right + hMargin, spouse.y - vMargin);
					ctx.lineTo(spouse.right + horizontalLengt, spouse.y - vMargin);
					ctx.stroke();
					spouseCenter = spouse.right + horizontalLengt;
				}
				else
				{
					ctx.moveTo(spouse.left - hMargin, spouse.y - vMargin);
					ctx.lineTo(spouse.left - horizontalLengt, spouse.y - vMargin);
					ctx.stroke();
					spouseCenter = spouse.left - horizontalLengt;
				}
			}
			
			if((person.exists || spouse.exists) && children.length > 0)
			{
				var childHorizontalY = person.y + (children[0].y - children[0].height - person.y) / 2;
				//vertical
				ctx.moveTo(spouseCenter, person.y - vMargin);
				ctx.lineTo(spouseCenter, childHorizontalY);
				ctx.stroke();
				families.push(new Family(spouseCenter, person.y - vMargin, familyUrl));
				//child horizontal
				ctx.moveTo(children[0].x, childHorizontalY);
				ctx.lineTo(spouseCenter, childHorizontalY);
				ctx.stroke();
				ctx.moveTo(children[children.length-1].x, childHorizontalY);
				ctx.lineTo(spouseCenter, childHorizontalY);
				ctx.stroke();
				
				//child verticals
				children.forEach(function(child)
				{
					ctx.moveTo(child.x, child.y - child.height);
					ctx.lineTo(child.x, childHorizontalY);
					ctx.stroke();
				});
			}
		}
		
		function handlePersonClick(posx, posy)
		{
			personClicked = false;
			persons.forEach(function(person)
			{
				if(person.isClicked(posx, posy))
				{
					personClicked = true;
					window.location.href = person.url;
				}
					
			});
			return personClicked;
		}
		
		function handleFamilyClick(posx, posy)
		{
			familyClicked = false;
			families.forEach(function(family)
			{
				if(family.isClicked(posx, posy))
				{
					familyClicked = true;
					window.location.href = family.url;
					//alert('is clicked');
				}
					
			});
			return familyClicked;
		}
		
		function handleButtonClick(posx, posy)
		{
			var buttonClicked = false;
			//sibling checkbox
			if(siblingButton)
			{
				if(siblingButton.isClicked(posx, posy))
				{
					buttonClicked = true;
					showSiblings = !showSiblings;
					clear();
					draw();
				}
			}
			
			//family radio buttons
			if(!buttonClicked)
			{
				var clickedButtonId = familyButtons.isClicked(posx, posy);

				if(clickedButtonId > -1)
				{
					buttonClicked = true;
					//action
					currentFamily = clickedButtonId;
					clear();
					draw();
				}
			}
			
			//parents radio buttons
			if(!buttonClicked)
			{
				var clickedButtonId = parentsButtons.isClicked(posx, posy);

				if(clickedButtonId > -1)
				{
					buttonClicked = true;
					//action
					currentParents = clickedButtonId;
					clear();
					draw();
				}
			}
			return buttonClicked;
		}
		
		function Person(personObj, ctx, alignment)
		{
			textHeight = 16;
			this.height = 2 * textHeight;
			this.width = 0;
			this.font = "12px 'arial'";
			this.exists = false;
			if(personObj)
			{
				if(personObj.isCurrent)
					this.font = "16px 'arial'";
				ctx.font = this.font;
				this.firstName = personObj.firstName;
				this.lastName = personObj.lastName;
				this.width = ctx.measureText(this.firstName).width;
				if(ctx.measureText(this.lastName).width > this.width)
					this.width = ctx.measureText(this.lastName).width;
				
				this.gender = personObj.gender;
				this.url = personObj.url;
				this.exists = true;
			}
			this.setNames = function(firstName, lastName)
			{
				this.firstName = firstName;
				this.lastName = lastName;
				this.width = ctx.measureText(this.firstName).width;
				if(ctx.measureText(this.lastName).width > this.width)
					this.width = ctx.measureText(this.lastName).width;
			};
			this.draw = function()
			{
				if(this.exists)
				{
					if(this.gender == 'M')
						ctx.fillStyle = settings.maleColor;
					else if(this.gender == 'F')
						ctx.fillStyle = settings.femaleColor;
					else
						ctx.fillStyle = settings.unknownColor;
					
					ctx.textAlign = "center";
					ctx.font = this.font;
					ctx.fillText(this.firstName, this.x, this.y - textHeight);
					ctx.fillText(this.lastName, this.x, this.y);
					//ctx.rect(this.x-1, this.y, 2, 2);//debug
				}
			};
			this.position = function(x, y)
			{
				this.x = Math.floor(x);
				this.y = Math.floor(y);
				this.left = Math.floor(x);
				this.right = Math.floor(x);
				
				if(alignment == 'left')
					this.x = Math.floor(this.x - this.width / 2);
				else if(alignment == 'right')
					this.x = Math.floor(this.x + this.width / 2);
				
				this.left = Math.floor(this.x - this.width / 2);
				this.right = Math.floor(this.x + this.width / 2);
			};
			this.isClicked = function(x, y)
			{
				if(x >= this.left && x <= this.right && y < this.y && y > this.y - this.height)
					return true;
				else
					return false;
			}
		}
		
		function Family(x, y, url)
		{
			this.url = url;
			this.left = x - (settings.hMargin-5);
			this.right = x + (settings.hMargin-5);
			this.top = y - (settings.hMargin-5);
			this.bottom = y + (settings.hMargin-5);
			this.isClicked = function(x, y)
			{
				if(x >= this.left && x <= this.right && y < this.bottom && y > this.top)
					return true;
				else
					return false;
			}
		}
		
		function Button(ctx, x, y, id, style, label)
		{
			this.x = x;
			this.y = y;
			this.id = id;
			this.style = style;
			this.label = label;
			this.radius = 5;
			this.selected = false;
			this.labelX = this.x + 2 * this.radius + 3;
			this.font = "12px 'arial'";
			ctx.font = this.font;
			this.right = this.labelX + ctx.measureText(this.label).width;
			
			this.draw = function()
			{
				ctx.beginPath();
				ctx.fillStyle = settings.buttonColor;
				ctx.strokeStyle = settings.buttonColor;
				centerX = this.x + this.radius;
				centerY = this.y - this.radius;
				width = 2 * this.radius;
				height = width;
				topLeftCornerX = this.x;
				topLeftCornerY = this.y - height;
				if(this.style == 'radio')
					ctx.arc(centerX, centerY, this.radius, 0, 2 * Math.PI, false);
				else if(this.style == 'checkbox')
					ctx.rect(topLeftCornerX, topLeftCornerY, width, height);
				if(this.selected)
					ctx.fill();
				ctx.stroke();
				
				ctx.textAlign = "left";
				ctx.font = this.font;
				ctx.fillText(this.label, this.labelX, centerY + 4);
			};
			this.setSelected = function(isSelected)
			{
				this.selected = isSelected;
			};
			this.isClicked = function(x, y)
			{
				var left = this.x;
				var right = this.x + 2 * this.radius;
				var top = this.y - 2 * this.radius;
				var bottom = this.y;
				if(x > left && x < right && y < bottom && y > top)
					return true;
				else
					return false;
			}
		}
		
		function RadioButtonGroup()
		{
			this.buttons = new Array();
			
			this.addButton = function(button)
			{
				this.buttons.push(button);
			};
			this.isClicked = function(posx, posy)
			{
				var clickedButtonId = -1;
				this.buttons.forEach(function(button)
				{
					if(button.isClicked(posx, posy))
					{
						//alert(button.id + ' is clicked');
						clickedButtonId = button.id;
					}
				});
				if(clickedButtonId > -1)
				{
					//change selected
					this.buttons.forEach(function(button)
					{
						//clear selected
						if(button.selected)
						{
							button.setSelected(false);
						}
					});
					this.buttons[clickedButtonId].setSelected(true);
				}
				return clickedButtonId;
			}
		}
		
		return this;
	}
})(jQuery);